'use strict';

const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerConfg = require('./swigger');

/** Carregamento das váriaveis de ambiente */
const env = require('dotenv');
env.config();

/** Carregamento de Drivers de Banco de Dados */
const mongoose = require('mongoose');

/** Carregamento de Middlewares */
const crossOrigin = require('./api/middleware/cross-origin');
const morgan = require('morgan');
const bodyParser = require('body-parser');

/** Carregamento de Rotas */
const usuarioRoute = require('./api/routes/usuario');

/** Conexão com o MongoDB */
console.log(process.env.MONGO_URI);
mongoose.connect(process.env.MONGO_URI, { useCreateIndex: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(crossOrigin);

/** Handling das rotas da API */
app.use('/usuario', usuarioRoute);

/** Handling da Documentação da API */
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerConfg.specs, swaggerConfg.options));

/**
 * Caso todos os uses anteirores não tenham tido sucesso a rota não foi encontrada.
 */
app.use((req, res, next) => {
    const error = new Error('Rota não encontrada');
    error.status = 404;
    next(error);
});

/**
 * Caso o erro não tenha um status definido (401, 404, etc.), significa que foi uma
 * falha geral sem previsão sistemica anterior.
 */
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
