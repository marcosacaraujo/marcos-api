/**
 *
 * Arquivo de rotas para objeto (Usuario)
 * Autor : Marcos Araújo (dev.marcosacaraujo@gmail.com)
 * Data  : 2019-12-07
 *
 */

'use strict';

const express = require('express');
const router = express.Router();

const usuarioController = require('../controllers/usuario');
const checkAuth = require('../middleware/check-auth');

/**
 * @swagger
 * /usuario/signup:
 *   post:
 *     tags:
 *       - Usuário
 *     description: Este endpoint deverá receber um Usuário como paramêtro.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Usuario
 *         in: body
 *         required: true
 *     responses:
 *       200:
 *         description: OK
 *       204:
 *         description: Paramêtros inválidos
 *       409:
 *         description: E-mail já existente
 *       500:
 *         description: Houve um erro inesperado
 */
router.post('/signup', usuarioController.signup);

/**
 * @swagger
 * /usuario/signin:
 *   post:
 *     tags:
 *       - Usuário
 *     description: Este endpoint irá receber um objeto com e-mail e senha.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Usuario
 *         description: Modelo Usuario
 *         in: body
 *         required: true
 *     responses:
 *       200:
 *         description: OK
 *       204:
 *         description: Paramêtros inválidos
 *       401:
 *         description: Usuário e/ou senha inválidos
 *       404:
 *         description: Usuário e/ou senha inválidos
 *       500:
 *         description: Houve um erro inesperado
 */
router.post('/signin', usuarioController.signin);

/**
 * @swagger
 * /usuario/{id}:
 *   get:
 *     tags:
 *       - Usuário
 *     description: Chamadas para este endpoint é autenticada com um token JWT.
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *           required: true
 *     responses:
 *       200:
 *         description: OK
 *       401:
 *         description: Usuário e/ou senha inválidos
 *       404:
 *         description: Usuário e/ou senha inválidos
 *       500:
 *         description: Houve um erro inesperado
 */
router.get('/:id', checkAuth, usuarioController.find);

module.exports = router;
