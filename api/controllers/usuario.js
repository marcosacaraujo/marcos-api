/**
 *
 * Arquivo de rotas para objeto (Usuario)
 * Autor : Marcos Araújo (dev.marcosacaraujo@gmail.com)
 * Data  : 2019-12-07
 *
 */

'use strict';

const mongoose = require('mongoose');
const Usuario = require('../models/usuario');
const validator = require('../validators/usuario');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

/** Controller responsável por criar novos Usuários */
exports.signup = (req, res, next) => {
    if (!validator.validateUserSignup(req)) res.status(200).json({ message: 'Formato de objeto inválido' });

    Usuario.findOne({ email: req.body.email })
        .then(usuario => {
            if (usuario) {
                return res.status(409).json({ message: 'E-mail já existente' });
            }

            bcrypt.hash(req.body.senha, saltRounds, function (err, hash) {
                if (err) {
                    return res.status(500).json({ message: 'Houve um erro inesperado' });
                }

                const newUsuario = new Usuario({
                    _id: new mongoose.Types.ObjectId(),
                    nome: req.body.nome,
                    email: req.body.email,
                    data_criacao: new Date(),
                    data_atualizacao: new Date(),
                    ultimo_login: new Date(),
                    telefones: [req.body.telefones]
                });

                let hashSenha = hash;

                jwt.sign({ usuario: newUsuario }, process.env.JWT_SECRETKEY, { expiresIn: '30m' }, (err, token) => {
                    if (err) {
                        return res.status(500).json({ message: 'Houve um erro inesperado' });
                    }

                    newUsuario.token = token;
                    newUsuario.senha = hashSenha;

                    // Salvando objeto no banco de dados (e retornando o usuario)
                    newUsuario.save().then(data => {
                        data = data.toObject();
                        delete data.senha;
                        res.status(200).json(data);
                    }).catch(err => {
                        res.status(500).json({
                            message: 'Falha ao gravar',
                            error: err
                        });
                    });
                });
            });
        }).catch(err => {
            return res.status(500).json({
                message: 'Falha para resgatar informações',
                error: err
            });
        });
};

/** Controller responsável por obter o token JWT e retornar o Usuário */
exports.signin = (req, res, next) => {
    if (!validator.validateSingIn(req)) res.status(200).json({ message: 'Formato de objeto inválido' });

    Usuario.findOne({ email: req.body.email })
        .then(usuario => {
            if (!usuario) {
                return res.status(404).json({ message: 'Usuário e/ou senha inválidos' });
            }

            bcrypt.compare(req.body.senha, usuario.senha, function (err, isValid) {
                if (err) {
                    return res.status(500).json({ message: 'Houve um erro inesperado' });
                }

                if (isValid !== true) {
                    return res.status(401).json({ message: 'Usuário e/ou senha inválidos' });
                } else {
                    usuario.data_atualizacao = new Date();
                    usuario.ultimo_login = new Date();
                    usuario.save();

                    usuario = usuario.toObject();
                    delete usuario.senha;
                    res.status(200).json(usuario);
                }
            });
        }).catch(err => {
            return res.status(500).json({
                message: 'Falha para resgatar informações',
                error: err
            });
        });
};

/** Controller responsável por obter o Usuário */
exports.find = (req, res, next) => {
    Usuario.findOne({ _id: req.params.id })
        .then(usuario => {
            if (!usuario) { res.status(404).json({ message: 'Não foi encontrad o usuário' }); };

            usuario = usuario.toObject();
            delete usuario.senha;

            res.status(200).json(usuario);
        })
        .catch(err => {
            return res.status(500).json({
                message: 'Falha para resgatar a informação',
                data: { id: req.params.id },
                error: err
            });
        });
};
