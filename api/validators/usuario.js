/**
 *
 * Arquivo de rotas para objeto (Usuario)
 * Autor : Marcos Araújo (dev.marcosacaraujo@gmail.com)
 * Data  : 2019-20-07
 *
 */

'use strict';

/** [Validator] - Validando o corpo do request para o cadastro do Usuário */
exports.validateUserSignup = (req) => {
    if (!req.body) {
        return false;
    }

    if (!req.body.hasOwnProperty('nome')) {
        return false;
    }

    if (!req.body.hasOwnProperty('email')) {
        return false;
    }

    if (!req.body.hasOwnProperty('senha')) {
        return false;
    }

    if (!req.body.hasOwnProperty('telefones')) {
        return false;
    }

    return true;
};

/** [Validator] - Validando o corpo do request para o Login do Usuário */
exports.validateSingIn = (req) => {
    if (!req.body) {
        return false;
    }

    if (!req.body.hasOwnProperty('email')) {
        return false;
    }

    if (!req.body.hasOwnProperty('senha')) {
        return false;
    }

    return true;
};
