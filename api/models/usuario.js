/**
 *
 * Arquivo de rotas para objeto (Usuario)
 * Autor : Marcos Araújo (dev.marcosacaraujo@gmail.com)
 * Data  : 2019-12-07
 *
 */

'use strict';

const mongoose = require('mongoose');

/** Modelo Usuario */
const usuarioSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nome: {
        type: String,
        validate: {
            validator: function (v) {
                return (v.length > 3);
            },
            message: '{VALUE} não correspondem as regras de nome de usuário.'
        }
    },
    email: {
        type: String,
        required: false,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    senha: { type: String },
    token: { type: String },
    ultimo_login: { type: Date },
    data_criacao: { type: Date },
    data_atualizacao: { type: Date },
    telefones: []
});

module.exports = mongoose.model('Usuario', usuarioSchema);
