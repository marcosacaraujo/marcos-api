/**
 *
 * Arquivo de rotas para objeto (Usuario)
 * Autor : Marcos Araújo (dev.marcosacaraujo@gmail.com)
 * Data  : 2019-12-07
 *
 */

'use strict';

const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.bearer;
        const decoded = jwt.verify(token, process.env.JWT_SECRETKEY);
        req.tokenJwt = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Não autorizado'
        });
    }
};
