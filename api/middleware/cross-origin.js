/**
 *
 * Arquivo de rotas para objeto (Usuario)
 * Autor : Marcos Araújo (dev.marcosacaraujo@gmail.com)
 * Data  : 2019-12-07
 *
 */

'use strict';

module.exports = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    next();
};
