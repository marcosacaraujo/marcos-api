/* eslint-disable handle-callback-err */
/* eslint-disable no-undef */
'use strict';

const Usuario = require('../api/models/usuario');
const server = require('../app');

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);

var newUsuario = {
    nome: 'Marcos Araújo',
    email: 'dev.marcosacaraujo@gmail.com',
    senha: 'OIJDSKDSJLSK',
    telefones: [{ numero: '123456789', ddd: '11' }]
};

describe('Testes de Integração [ Cadastro ]', () => {
    beforeEach((done) => {
        Usuario.deleteMany((err) => { done(); });
    });
    it('Cadastrando um usuário', (done) => {
        chai.request(server)
            .post('/usuario/signup')
            .send(newUsuario)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id');
                res.body.should.have.property('data_criacao');
                res.body.should.have.property('data_atualizacao');
                res.body.should.have.property('ultimo_login');
                res.body.should.have.property('token');

                res.body.should.have.property('nome');
                res.body.should.have.property('email');
                res.body.should.have.property('telefones');

                newUsuario = res.body;
                done();
            });
    });
});

describe('Testes de Integração [ Login, Consultas ]', () => {
    it('Realizando o login do Usuário', (done) => {
        const login = {
            email: 'dev.marcosacaraujo@gmail.com',
            senha: 'OIJDSKDSJLSK'
        };
        chai.request(server)
            .post('/usuario/signin')
            .set('Accept', 'application/json')
            .send(login)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id');
                res.body.should.have.property('data_criacao');
                res.body.should.have.property('data_atualizacao');
                res.body.should.have.property('ultimo_login');
                res.body.should.have.property('token');

                res.body.should.have.property('nome');
                res.body.should.have.property('email');
                res.body.should.have.property('telefones');

                newUsuario = res.body;
                done();
            });
    });

    it('Procura o usuário por Id', (done) => {
        let id = newUsuario._id;
        let token = newUsuario.token;
        chai.request(server)
            .get(`/usuario/${id}`)
            .set('bearer', token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id');
                res.body.should.have.property('data_criacao');
                res.body.should.have.property('data_atualizacao');
                res.body.should.have.property('ultimo_login');
                res.body.should.have.property('token');

                res.body.should.have.property('nome');
                res.body.should.have.property('email');
                res.body.should.have.property('telefones');
                done();
            });
    });
});
