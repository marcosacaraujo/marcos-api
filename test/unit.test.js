/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
'use strict';

const expect = require('chai').expect;
const usuarioValidator = require('../api/validators/usuario');

describe('Teste Unitário - [Validators]', function () {
    it('[signup] - Validando corpo do request para o Cadastro de usuário', () => {
        let req = {
            body: {
                nome: 'Marcos Araújo',
                email: 'dev.marcosacaraujo@gmail.com',
                senha: 'OIJDSKDSJLSK',
                telefones: [{ numero: '123456789', ddd: '11' }]
            }
        };
        let result = usuarioValidator.validateUserSignup(req);
        expect(result).to.be.true;
    });

    it('[signin] - Validando corpo do request para o Login', () => {
        let req = {
            body: {
                email: 'dev.marcosacaraujo@gmail.com',
                senha: 'OIJDSKDSJLSK'
            }
        };
        let result = usuarioValidator.validateSingIn(req);
        expect(result).to.be.true;
    });
});
