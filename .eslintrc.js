module.exports = {
    parser: "esprima",
    extends: ["standard"],
    env: {
        "node": true
    },
    plugins: [

    ],
    rules: {
        indent: [ "error", 4 ],
        "semi-style": ["error", "last"],
        "semi": ["error", "always"],
        "no-unused-vars": ["warn", { "vars": "all", "args": "none" }]
    }

};

