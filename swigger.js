const swaggerJsdoc = require('swagger-jsdoc');

/** Opções de Informações da UI da Swigger */
exports.specs = swaggerJsdoc({
    swaggerDefinition: {
        info: {
            title: 'Marcos-API',
            version: '1.0.0',
            description: 'API desenvolvida para Teste de conhecimento em Nodejs.',
            termsOfService: 'https://creativecommons.org/licenses/by-nc-nd/4.0/',
            contact: {
                email: 'dev.marcosacaraujo@gmail.com',
                url: 'https://www.linkedin.com/in/macaraujo/'
            },
            license: {
                name: 'Apache 2.0',
                url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
            }
        },
        securityDefinitions: {
            Bearer: {
                type: "apiKey",
                name: "Bearer",
                in: "header"
            }
        }
    },
    apis: ['./api/routes/*.js', './api/models/*js']
});

/** Opções de Auth do Swigger */
exports.options = {
    swaggerOptions: {
        authAction: { JWT: { name: "JWT", schema: { type: "apiKey", in: "header", name: "Authorization", description: "" }, value: "Bearer <JWT>" } }
    }
};
