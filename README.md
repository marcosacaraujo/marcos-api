# Marcos-API

API desenvolvida para Teste de conhecimento em Nodejs. Neste projeto atenderá todos os requisitos do teste. Obrigado ;)

# DOC
* [Documentação da API usando Swigger](https://api.hawiio.com/docs)

# Tecnologias

* [Express](http://expressjs.com/pt-br/)
* [ESLint](https://eslint.org/docs/user-guide/getting-started)

**Portas usadas**: estas portas devem estar livres no Webserver para que seja possível efetuar acessar os serviços WEB.

* 3000 - Express

# Bancos de Dados

* [MongoDB](https://www.sqlite.org/index.html)

# Teste de Unidade e Integração:

Para garantir a qualidade do código, execute este comando abaixo em desenvolvimento antes de fazer um merge request na branch *master*:

```
$ npm run quality
...
```